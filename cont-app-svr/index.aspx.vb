﻿Imports MySql.Data.MySqlClient
Public Class servicio
    Inherits System.Web.UI.Page
    Dim cn As New MySqlConnection("server=localhost;user id=root;persistsecurityinfo=True;database=contabilidad;password=cc123456;SslMode=none")
    Dim cmd As MySqlCommand
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim result As String = "<h1>RAESoft CONT-APP</h1><hr/>"
        Dim f As Object = Request.QueryString("f"), json As String = Request.QueryString("json")
        If f Is Nothing Then Response.Write(result) : Exit Sub
        If f > 0 And logueado() = False Then
            result = sAlert("type:error,title:Sesión expirada")
            GoTo contestar
        End If
        If cn.State = ConnectionState.Closed Then cn.Open()
        Select Case f
            Case 0 'inciar sesion
                result = iniciarSesión(json)
            Case 1 'obtener catálogo
                result = obtenerCatálogo()
            Case 2 'guardar cuenta
                result = guardarCuenta(json)
            Case 3 'eliminar cuenta
                result = eliminarCuenta(json)
            Case 4 'guardar asiento
                Dim asiento As String = Request.QueryString("asiento")
                Dim filas As String = Request.QueryString("filas")
                result = guardarAsiento(asiento, filas)
            Case 5 'obtener listas
                Dim nombreLista As String = Request.QueryString("nombre")
                result = obtenerListas(nombreLista)
            Case 6 'agregarReferencia
                Dim ref As String = Request.QueryString("ref")
                result = agregarReferencia(ref)

        End Select
contestar:
        cn.Close()
        Response.Write(result)
    End Sub
    Private Function iniciarSesión(ByVal json As String) As String

        Dim result As String = ""
        Dim js As New Script.Serialization.JavaScriptSerializer
        Dim obj As Dictionary(Of String, Object) = js.Deserialize(json, GetType(Object))
        Dim usuario As String = obj("usuario")
        Dim pass As String = obj("pass")
        Dim t As DataTable = query("select * from usuarios where usuario='" + usuario + "'")
        If t.Rows.Count = 0 Then
            result = sAlert("type:error,title:Error al iniciar sesión,text:El usuario no existe")
        Else
            t = query("select * from usuarios where Usuario='" + usuario + "' and Contraseña='" + pass + "'")
            If t.Rows.Count = 0 Then
                result = sAlert("type:error,title:Error al iniciar sesión,text:Contraseña incorrecta")
            Else
                Application("userInfo") = t.Rows(0).ItemArray
                result = sAlert("type:success")
            End If
        End If
        Return result
    End Function

    Private Function obtenerCatálogo(Optional ByVal nombreas As String = "") As String
        Dim result As String = ""
        Dim t As DataTable = query("select * from catálogo where instr(Código,'-')=0")
        result = tabla2JSON(t)
        result = sAlert("type:success", result)
        Return result
    End Function
    Public Function obtenerListas(Optional ByVal nombreLista As String = "") As String
        Dim result As String = ""
        If nombreLista.Length > 0 Then
            Dim t As DataTable = query("select id,nombre from `" + nombreLista + "`")
            If t.Columns(0).ColumnName = "error" Then
                result = sAlert("type:error,text:" + t.Rows(0)(0).ToString.Replace("'", ""))
            Else
                For Each f As DataRow In t.Rows
                    result += "{" + Chr(34) + "id" + Chr(34) + ":" + f.Item("id").ToString + "," + Chr(34) + "nombre" + Chr(34) + ":" + Chr(34) + f.Item("nombre") + Chr(34) + "},"
                Next
                If result.Length > 0 Then
                    result = sAlert("type:success", "[" + Left(result, result.Length - 1) + "]")
                Else
                    result = sAlert("type:success", "[]")
                End If
            End If
        End If
        Return result
    End Function
    Private Function eliminarCuenta(ByVal json As String) As String
        Dim result As String = ""
        Dim js As New Script.Serialization.JavaScriptSerializer
        Dim obj As Object = js.Deserialize(json, GetType(Object))
        Dim sql As String = ""
        For Each kv As KeyValuePair(Of String, Object) In obj
            sql += "delete from catálogo where Código='" + kv.Value + "';"
        Next
        Dim myT As DataTable = query(sql)
        If myT Is Nothing Then
            result = sAlert("type:error,text:Error al eliminar cuenta")
        Else
            result = sAlert("type:success")
        End If
        Return result
    End Function
    Private Function tabla2JSON(tabla As DataTable) As String
        If tabla.Rows.Count = 0 Then Return "[]"
        Dim result As String = ""
        For Each fila As DataRow In tabla.Rows
            Dim keys As String = ""
            For Each col As DataColumn In tabla.Columns
                If col.DataType = GetType(Int32) Or col.DataType = GetType(Double) Then
                    Dim valor As String = fila(col.ColumnName).ToString
                    keys += Chr(34) + col.ColumnName + Chr(34) + ":" + valor + ","
                Else
                    keys += Chr(34) + col.ColumnName + Chr(34) + ":" + Chr(34) + fila(col).ToString + Chr(34) + ","
                End If
            Next
            keys += Chr(34) + "abierto" + Chr(34) + ":" + "false,"
            Dim sql As String = "select * from catálogo where LEFT(Código,LENGTH('" + fila("Código") + "')+1)='" + fila("Código") + "-' and (INSTR(MID(Código,LENGTH('" + fila("Código") + "')+2),'-')=0)"
            Dim newT As DataTable = query(sql)
            result += "{" + keys + Chr(34) + "sub" + Chr(34) + ":" + tabla2JSON(newT) + "},"
        Next
        result = "[" + Left(result, result.Length - 1) + "]"
        Return result
    End Function
    Private Function guardarCatálogo(ByVal json As String) As String
        Dim result As String = ""
        Dim js As New Script.Serialization.JavaScriptSerializer
        Dim obj As Object = js.Deserialize(json, GetType(Object))
        Try
            runSQL("delete from catalogo where Código='" + obj.Código + "'")
            result = sAlert("type:success")
        Catch ex As Exception
            result = sAlert("type:error,text:" + ex.Message.Replace(Chr(34), "''").Replace(":", "==").Replace(",", ";;"))
        End Try
        Return result
    End Function
    Private Function guardarCuenta(ByVal json As String) As String
        Dim result As String = ""
        Dim cmd As MySqlCommand = cn.CreateCommand
        Dim sql As String = obtenerSQL(json, "catálogo", "Código")
        cmd.CommandText = sql
        Try
            cmd.ExecuteNonQuery()
            result = sAlert("type:success,text:Cuenta guardada exitósamente,title:Guardado!!!")
        Catch ex As Exception
            result = sAlert("type:error,text:" + ex.Message.Replace(Chr(34), "''").Replace(":", "==").Replace(",", ";;"))
        End Try
        Return result
    End Function
    Private Function guardarAsiento(ByVal asiento As String, filas As String) As String
        Dim result As String = sAlert("type:error,title:Algo salió mal,text:No se pudo guardar el asiento")
        Try
            Dim sql As String = obtenerSQL2(asiento, "asientos de diario", "id") + ";select last_insert_id();"
            Dim t As DataTable = query(sql)
            sql = ""
            If t.Rows.Count > 0 Then
                Dim id As String = t.Rows(0)(0).ToString
                runSQL("delete from `detalle de asiento` where id=" + id)
                Dim js As New Script.Serialization.JavaScriptSerializer
                Dim objFilas = js.Deserialize(filas, GetType(Object))
                For Each fila In objFilas
                    Dim campos As String = "", valores As String = ""
                    For Each kv In fila
                        If kv.key = "Cuenta" Then Continue For
                        If kv.key = "Código" Then
                            campos += "`Cuenta`,"
                        Else
                            campos += "`" + kv.key + "`,"
                        End If
                        valores += "'" + kv.value.ToString + "',"
                    Next
                    campos += "`Asiento`"
                    valores += id
                    sql += "insert into `detalle de asiento`(" + campos + ") values(" + valores + ");"
                Next
                result = runSQL(sql)
                result = sAlert("type:success,title:Asiento guardado correctamente")
            End If
        Catch ex As Exception
            result = sAlert("type:error,text:" + ex.Message.Replace(Chr(34), "").Replace("'", ""))
        End Try

        Return result
    End Function

    Private Function agregarReferencia(ByVal ref As String) As String
        Dim result As String = ""
        Dim sql As String = "insert into `referencias de transacción`(nombre) values('" + ref + "');select last_insert_id()"
        Dim t As DataTable = query(sql)
        If t.Columns(0).ColumnName = "error" Then
            result = sAlert("type:error,text:" + t.Rows(0)(0).ToString.Replace(Chr(34), "''"))
        Else
            result = sAlert("type:success", t.Rows(0)(0).ToString)
        End If
        Return result
    End Function
    'Devuelve true si se encuentra un registro con la clave en el parámetro código
    Private Function códigoRepetido(ByVal código As String, clave As String) As Boolean
        Dim cmd As MySqlCommand = cn.CreateCommand
        cmd.CommandText = "select 1 from catálogo where " + clave + "='" + código + "'"
        Try
            Dim r As Integer = cmd.ExecuteScalar
            If r = 1 Then Return True Else Return False
        Catch ex As Exception
            Return False
        End Try
    End Function

    'Desde un string json devuelve una cadena de actualización o inserción a una tabla específica
    Function obtenerSQL(ByVal json As String, tabla As String, clave As String) As String
        Dim result As String = "", campos As String = "", valores As String = ""
        Dim js As New Script.Serialization.JavaScriptSerializer
        Dim jsObj As Object = js.Deserialize(json, GetType(Object))
        For Each kv As Object In jsObj
            If kv.key = "abierto" Or kv.key = "sub" Then Continue For
            campos += "`" + kv.Key + "`,"
            valores += "'" + kv.Value.ToString + "',"
            result += "`" + kv.Key + "`='" + kv.Value.ToString + "',"
        Next
        If códigoRepetido(jsObj(clave), clave) Then
            result = "update `" + tabla + "` set " + Left(result, result.Length - 1) + " where `" + clave + "`='" + jsObj(clave) + "'"
        Else
            result = "insert into `" + tabla + "`(" + Left(campos, campos.Length - 1) + ") values(" + Left(valores, valores.Length - 1) + ")"
        End If
        Return result
    End Function
    Function obtenerSQL2(ByVal json As String, tabla As String, clave As String) As String
        Dim result As String = "", campos As String = "", valores As String = ""
        Dim js As New Script.Serialization.JavaScriptSerializer
        Dim jsObj As Object = js.Deserialize(json, GetType(Object))
        For Each kv As Object In jsObj
            If kv.key = clave Then Continue For
            campos += "`" + kv.Key + "`,"
            valores += "'" + kv.Value.ToString + "',"
            result += "`" + kv.Key + "`='" + kv.Value.ToString + "',"
        Next
        If códigoRepetido(jsObj(clave), clave) Then
            result = "update `" + tabla + "` set " + Left(result, result.Length - 1) + " where `" + clave + "`='" + jsObj(clave) + "'"
        Else
            result = "insert into `" + tabla + "`(" + Left(campos, campos.Length - 1) + ") values(" + Left(valores, valores.Length - 1) + ")"
        End If
        Return result
    End Function

    'Convierte una cadena en formato json para enviarlo a un sweetalert2
    Private Function sAlert(def As String, ByVal Optional agr As String = "") As String
        Dim result As String = ""
        Dim m() As String = def.Split(",")
        For Each kv As String In m
            Dim item() As String = kv.Split(":")
            result += Chr(34) + item(0) + Chr(34) + ":" + Chr(34) + item(1).ToString.Replace(";;", ",").Replace("==", ":") + Chr(34) + ","
        Next
        If agr.Length > 0 Then agr = "," + Chr(34) + "agregado" + Chr(34) + ":" + agr
        result = "{" + Left(result, result.Length - 1) + agr + "}"
        Return result
    End Function

    'Obtiene un datatable con los parámetros sql correspondientes
    Private Function query(ByVal sql As String) As DataTable
        Dim result As New DataTable
        Dim cmd As MySqlCommand = cn.CreateCommand
        Dim adp As New MySqlDataAdapter
        adp.SelectCommand = cmd
        cmd.CommandText = sql
        Try
            adp.Fill(result)
        Catch ex As Exception
            Console.WriteLine(ex.Message)
            result.Columns.Add("error")
            result.Rows.Add()
            result.Rows(0)(0) = ex.Message
        End Try
        cmd.Dispose()
        adp.Dispose()
        Return result
    End Function
    Private Function runSQL(ByVal sql As String) As String
        Dim result As String = ""
        Dim cmd As MySqlCommand = cn.CreateCommand
        cmd.CommandText = sql
        Try
            result = cmd.ExecuteScalar()
        Catch ex As Exception
            result = ex.Message
            MsgBox(ex.Message)
        End Try
        cmd.Dispose()
        Return result
    End Function
    Private Function logueado() As Boolean
        Dim result As Boolean = False
        Dim infoU As Array = Application("userInfo")
        If Not infoU Is Nothing Then
            result = True
        End If
        Return result
    End Function
End Class